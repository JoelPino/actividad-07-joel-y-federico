import java.util.ArrayList;

public class Catalogo {
	private final ArrayList<Filmografia> peliculaseries = new ArrayList<>();
	
	public static void main (String [] ars){
		Catalogo Catalogo = new Catalogo();
		Catalogo.cargar();
		Catalogo.mostrar();
	}
	private void cargar(){
		peliculaseries.add(new Pelicula("Top Secret!","Comedia","Desconocido" ));
		peliculaseries.add(new Pelicula("The Truman Show", "Drama", "1998"));
		peliculaseries.add(new Pelicula("Wind Rive","Drama","2017" ));
		peliculaseries.add(new Pelicula("Avengers: Infinity War", "Acción", "2018"));
		peliculaseries.add(new Serie("Friends", "Sitcom", "1994", "10"));
		peliculaseries.add(new Serie("Breaking Bad","Drama","2008", "7"));
		peliculaseries.add(new Serie("Scrubs", "Comedia","2001", "6"));
	}
	private void mostrar(){
		int min = 10001;
		int max = 149;
                String titulomax = "";
                String titulomin = "";
		for (Filmografia i: peliculaseries){
			if(min>i.getEspectadores()){
                                titulomin= i.titulo;
				min = i.getEspectadores();
			}
			if(max<i.getEspectadores()){
                                titulomax = i.titulo;
				max = i.getEspectadores();
			}
		}
                System.out.println("--------");
		System.out.println("Mâs Taquillera: "+titulomax+" ("+max+" Espectadores)");
		System.out.println("Menor Taquillera: "+titulomin+" ("+min+" Espectadores)");
                System.out.println("--------");
                peliculaseries.forEach((i) -> {
                    System.out.println(i);
                });
	}
}
	

        