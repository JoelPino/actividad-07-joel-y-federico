
import java.util.Random;

public class Filmografia implements Comparable<Filmografia> {

    protected String titulo;
    protected String genero;
    protected String año;
    public int max = 10001;
    public int min = 149;
    Random especRandom = new Random();
    protected int espectadores = especRandom.nextInt(max - min) + min;

    public Filmografia(String titulo, String genero, String año) {
        this.titulo = titulo;
        this.genero = genero;
        this.año = año;
    }

    public int getEspectadores() {
        return espectadores;
    }
    public String getTitulo(){
        return titulo;
    }
    @Override
    public int compareTo(Filmografia o) {
        if (espectadores == o.espectadores) {
            return 0;
        } else if (espectadores > o.espectadores) {
            return 1;
        } else {
            return -1;
        }
    }
}
