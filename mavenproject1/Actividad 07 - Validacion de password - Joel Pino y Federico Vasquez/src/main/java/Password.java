
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Joe
 */
public class Password {

    private String password;
    private String mensaje;
    private int tamañopassowrd;
    private int caracter;
    private int digito;
    private final ArrayList<String> errores = new ArrayList<String>();

    public void cargarpassword(String password) {
        this.password = password;
    }

    public String validarpassword() {
        tamañopassowrd = password.length();
        digito = 0;
        mensaje = "4";
        if (password == "") {

            mensaje = "0";
            errores.add(mensaje);
        }
        if (password != "") {
            if (tamañopassowrd < 8) {

                mensaje = "1";
                errores.add(mensaje);
            }
            if (tamañopassowrd > 30) {

                mensaje = "2";
                errores.add(mensaje);
            }

            for (caracter = 0; caracter < tamañopassowrd; caracter++) {
                if (Character.isDigit(password.charAt(caracter))) {
                    digito = 1;
                }
            }
            if (digito == 0) {
                mensaje = "3";
                errores.add(mensaje);
            }

        }
        return mensaje;
    }

    public void mostrarmensaje() {
        if (mensaje == "4") {
            System.out.print("La Password es correcta");
        } else {
            for (int i = 0; i < errores.size(); i++) {
                System.out.println("Se ha generado el error: " + errores.get(i));
            }
        }
    }
}
