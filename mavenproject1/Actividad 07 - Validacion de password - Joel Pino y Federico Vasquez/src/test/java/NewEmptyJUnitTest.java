/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joe
 */
public class NewEmptyJUnitTest {

    private String esperado;
    private String resultado;
    Password password = new Password();

    public NewEmptyJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validarpassword_passwordnull_error0() {
        password.cargarpassword("");
        esperado = "0";
        resultado = password.validarpassword();
        assertEquals(esperado, resultado);
    }
    @Test
    public void validarpassword_passwordmin_error1() {
        password.cargarpassword("asdasd1");
        esperado = "1";
        resultado = password.validarpassword();
        assertEquals(esperado, resultado);
    }
    @Test
    public void validarpassword_passwordmax_error2() {
        password.cargarpassword("asdasd1asdasdasdasdasdasdasdasdasdasd");
        esperado = "2";
        resultado = password.validarpassword();
        assertEquals(esperado, resultado);
    }
    @Test
    public void validarpassword_passwordnumero_error3() {
        password.cargarpassword("asdasd");
        esperado = "3";
        resultado = password.validarpassword();
        assertEquals(esperado, resultado);
    }
    @Test
    public void validarpassword_passwordvalido_mensaje4() {
        password.cargarpassword("asdasasdasdad1");
        esperado = "4";
        resultado = password.validarpassword();
        assertEquals(esperado, resultado);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
